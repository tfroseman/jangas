package com.thomasroseman.java.apigee;

import static org.junit.Assert.assertTrue;

import com.thomasroseman.java.apigee.apigee.Apigee;
import com.thomasroseman.java.apigee.apigee.PublicCloud;
import com.thomasroseman.java.apigee.organization.Environment;
import com.thomasroseman.java.apigee.organization.Organization;
import com.thomasroseman.java.apigee.util.API_URL;
import org.junit.Test;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;

public class PublicCloudTest {

    PublicCloud apigee = new PublicCloud("", "");

    @BeforeEach
    void setUp() {
    }

    @AfterEach
    void tearDown() {
    }

    @Test
    public void shouldAnswerWithTrue() {
        //Organization organization = apigee.GetOrg("");
        //System.out.println(organization);
        //organization.fetchEnvironment(organization.getEnvironments().get(0));
        assertTrue(true);
    }

    @Test
    public void apigeeMock200() {
        //JsonResponse jsonResponse = apigee.getGenericJson(API_URL.MOCK_TARGET+API_URL.RETURN_STATUS+"200");
        JsonResponse jsonResponse = apigee.getGenericJson("https://mocktarget.apigee.net/statuscode/200");
        //System.out.println(jsonResponse);
    }

    @Test
    public void emptyApigeeURL() {
        assertTrue(true);
    }
}
