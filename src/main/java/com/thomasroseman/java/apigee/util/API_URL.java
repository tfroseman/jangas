package com.thomasroseman.java.apigee.util;

public class API_URL {

    public static final String PUBLIC_BASE_URL = "https://api.enterprise.apigee.com/";
    public static final String VERSION = "v1/";
    public static final String ORGANIZATION = PUBLIC_BASE_URL + "organizations/";
    public static final String ENVIRONMENT = ORGANIZATION + "%s" + "/environments/";
    public static final String API = ORGANIZATION + "%s/apis/";
    public static final String ORGANIZATION_DEPLOYMENTS = ORGANIZATION + "%s/deployments/";
    public static final String PODS = ORGANIZATION + "%s/pods/";

    // Mock Target
    public static final String MOCK_TARGET = "https://mocktarget.apigee.net/";
    public static final String RETURN_STATUS = MOCK_TARGET + "statuscode/";
}
