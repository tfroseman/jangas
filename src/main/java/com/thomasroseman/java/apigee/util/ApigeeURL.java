package com.thomasroseman.java.apigee.util;

import com.google.api.client.http.GenericUrl;

public class ApigeeURL extends GenericUrl {

        public ApigeeURL(String encodedUrl) {
            super(encodedUrl);
        }

}
