package com.thomasroseman.java.apigee.util;

import com.google.api.client.http.*;
import com.google.api.client.http.javanet.NetHttpTransport;
import com.google.api.client.json.JsonFactory;
import com.google.api.client.json.JsonObjectParser;
import com.google.api.client.json.jackson2.JacksonFactory;
import com.thomasroseman.java.apigee.JsonResponse;
import com.thomasroseman.java.apigee.organization.Organization;

import javax.annotation.CheckForNull;
import java.io.IOException;

public class HttpHelper {
    private String username, password;

    static final HttpTransport HTTP_TRANSPORT = new NetHttpTransport();
    static final JsonFactory JSON_FACTORY = new JacksonFactory();

    public HttpHelper(String username, String password) {
        this.username = username;
        this.password = password;
    }

    public HttpRequest request;
    public HttpRequestFactory requestFactory =
            HTTP_TRANSPORT.createRequestFactory(
                    new HttpRequestInitializer() {
                        @Override
                        public void initialize(HttpRequest httpRequest) throws IOException {
                            httpRequest.setParser(new JsonObjectParser(JSON_FACTORY));
                            httpRequest.setHeaders(new HttpHeaders().setAccept("application/json")
                                    .setBasicAuthentication(username, password));
                        }
                    }
            );

    public ApigeeURL newURL(String url){
        return new ApigeeURL(url);
    }


    public Object newGetRequest(ApigeeURL url, Class parsedClass){
        try {
            request = requestFactory.buildGetRequest(url);
            return request.execute().parseAs(parsedClass);
        } catch (IOException e) {
            e.printStackTrace();
        }

        return null;
    }

    public JsonResponse newGetRequestWithJson(ApigeeURL url) throws IOException {
            request = requestFactory.buildGetRequest(url);
            return (JsonResponse) execute(request);
    }

    private Object execute(HttpRequest request) throws IOException {
        HttpResponse response = null;
        try {
            response = request.execute();
        } catch (Exception e) {
            if ( e instanceof HttpResponseException ) {
                return response.parseAs(JsonResponse.class);
            }
            e.printStackTrace();
        }
            return response.parseAs(JsonResponse.class);
    }

    public Object newDeleteRequestWithJson(ApigeeURL url){
        try {
            request = requestFactory.buildDeleteRequest(url);
            return request.execute().parseAs(JsonResponse.class);
        } catch (IOException e) {
            e.printStackTrace();
        }

        return null;
    }

    public Object newPostRequest(ApigeeURL url, Class parsedClass, String body){
        try {
            request = requestFactory.buildPostRequest(url, ByteArrayContent.fromString(null,body));
            return request.execute().parseAs(parsedClass);
        } catch (IOException e) {
            e.printStackTrace();
        }

        return null;
    }

    public Object newPostRequestWithJson(ApigeeURL url, String body){
        try {
            request = requestFactory.buildPostRequest(url, ByteArrayContent.fromString(null,body));
            return request.execute().parseAs(JsonResponse.class);
        } catch (IOException e) {
            e.printStackTrace();
        }

        return null;
    }
}
