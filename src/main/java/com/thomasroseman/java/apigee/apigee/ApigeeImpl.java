package com.thomasroseman.java.apigee.apigee;

import com.google.api.client.http.*;
import com.google.api.client.http.javanet.NetHttpTransport;
import com.google.api.client.json.JsonFactory;
import com.google.api.client.json.JsonObjectParser;
import com.google.api.client.json.jackson2.JacksonFactory;
import com.google.api.client.util.Key;
import com.thomasroseman.java.apigee.JsonResponse;
import com.thomasroseman.java.apigee.organization.Environment;
import com.thomasroseman.java.apigee.organization.Organization;
import com.thomasroseman.java.apigee.organization.Properties;
import com.thomasroseman.java.apigee.util.API_URL;
import com.thomasroseman.java.apigee.util.ApigeeURL;
import com.thomasroseman.java.apigee.util.HttpHelper;

import java.io.IOException;
import java.util.ArrayList;

public abstract class ApigeeImpl implements Apigee {
    public Organization organization;
    HttpHelper httpHelper;

    public ApigeeImpl(String username, String password) {
        httpHelper = new HttpHelper(username, password);
    }

    public abstract Organization GetOrg(String organization);

    public abstract Organization UpdateOrgProperties(Properties properties);

    public abstract Environment GetEnv(String environment);

    public JsonResponse getGenericJson(String urlRequest){
        ApigeeURL url = httpHelper.newURL(urlRequest);

        try {
            return (JsonResponse) httpHelper.newGetRequestWithJson(url);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }
}
