package com.thomasroseman.java.apigee.apigee;

import com.thomasroseman.java.apigee.JsonResponse;
import com.thomasroseman.java.apigee.organization.Environment;
import com.thomasroseman.java.apigee.organization.Organization;
import com.thomasroseman.java.apigee.organization.Properties;

public class PrivateCloud extends ApigeeImpl {
    public PrivateCloud(String username, String password) {
        super(username, password);
    }

    @Override
    public Organization GetOrg(String organization) {
        return null;
    }

    @Override
    public Organization UpdateOrgProperties(Properties properties) {
        return null;
    }

    @Override
    public Environment GetEnv(String environment) {
        return null;
    }

    @Override
    public JsonResponse getGenericJson(String url) {
        return null;
    }
}
