package com.thomasroseman.java.apigee.apigee;

import com.thomasroseman.java.apigee.JsonResponse;
import com.thomasroseman.java.apigee.organization.Organization;

import java.util.ArrayList;

public interface Apigee {
    public Organization organization = new Organization();
    Organization GetOrg(String organization);

    JsonResponse getGenericJson(String url);
}
