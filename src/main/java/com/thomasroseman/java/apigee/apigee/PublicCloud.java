package com.thomasroseman.java.apigee.apigee;

import com.google.api.client.http.HttpContent;
import com.thomasroseman.java.apigee.JsonResponse;
import com.thomasroseman.java.apigee.organization.Environment;
import com.thomasroseman.java.apigee.organization.Organization;
import com.thomasroseman.java.apigee.organization.Properties;
import com.thomasroseman.java.apigee.util.API_URL;
import com.thomasroseman.java.apigee.util.ApigeeURL;
import com.thomasroseman.java.apigee.util.HttpHelper;

import java.io.IOException;
import java.io.OutputStream;
import java.util.ArrayList;

public class PublicCloud extends ApigeeImpl {


    public PublicCloud(String username, String password) {
        super(username, password);
    }

    @Override
    public Organization GetOrg(String org) {
        ApigeeURL url = httpHelper.newURL(API_URL.ORGANIZATION+"/"+org);
        this.organization = (Organization) httpHelper.newGetRequest(url, Organization.class);

        return this.organization;
    }

    @Override
    public Organization UpdateOrgProperties(Properties properties) {
        ApigeeURL url = httpHelper.newURL(API_URL.ORGANIZATION+ "/" + organization);

        this.organization = (Organization) httpHelper.newPostRequest(url,Organization.class, null);
        return this.organization;
    }

    @Override
    public Environment GetEnv(String environment) {
        return null;
    }

}
