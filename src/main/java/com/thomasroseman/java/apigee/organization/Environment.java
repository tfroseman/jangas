package com.thomasroseman.java.apigee.organization;

import java.util.ArrayList;
import java.util.Date;

public class Environment {
    private Date createAt;
    private String createdBy;
    private Date lastMondifiedAt;
    private String lastModifiedBy;
    private ArrayList<Properties> properties;
}
