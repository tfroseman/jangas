package com.thomasroseman.java.apigee.organization;

import com.google.api.client.util.Key;

import java.util.ArrayList;

public class Organization{
    @Key private Long createAt;
    @Key private String createdBy;
    @Key private String displayName;
    @Key private ArrayList<String> environments;
    @Key private Long lastMondifiedAt;
    @Key private String lastModifiedBy;
    @Key private Properties properties;
    @Key private String name;
    @Key private String type;

    public Organization() {
    }

    public Long getCreateAt() {
        return createAt;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public String getDisplayName() {
        return displayName;
    }

    public ArrayList<String> getEnvironments() {
        return environments;
    }

    public Long getLastMondifiedAt() {
        return lastMondifiedAt;
    }

    public String getLastModifiedBy() {
        return lastModifiedBy;
    }

    public Properties getProperties() {
        return properties;
    }

    public String getName() {
        return name;
    }

    public String getType() {
        return type;
    }

    @Override
    public String toString() {
        return "Organization{" +
                "createAt=" + createAt +
                ", createdBy='" + createdBy + '\'' +
                ", displayName='" + displayName + '\'' +
                ", Environments=" + environments +
                ", lastMondifiedAt=" + lastMondifiedAt +
                ", lastModifiedBy='" + lastModifiedBy + '\'' +
                ", properties=" + properties +
                ", name='" + name + '\'' +
                ", type='" + type + '\'' +
                '}';
    }



    public Environment fetchEnvironment(String env) {
        return null;
    }
}
