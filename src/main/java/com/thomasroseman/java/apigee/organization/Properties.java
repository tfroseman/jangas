package com.thomasroseman.java.apigee.organization;

import com.google.api.client.util.Key;

import java.util.ArrayList;

public class Properties {

    @Key private ArrayList<Property> property;

    @Override
    public String toString() {
        return "Properties{" +
                "property=" + property +
                '}';
    }
}
