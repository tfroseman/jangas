package com.thomasroseman.java.apigee.organization;

import com.google.api.client.util.Key;

public class Property {

        @Key
        private String name;
        @Key private String value;

        public Property() {
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getValue() {
            return value;
        }

        public void setValue(String value) {
            this.value = value;
        }

        @Override
        public String toString() {
            return "property{" +
                    "name='" + name + '\'' +
                    ", value='" + value + '\'' +
                    '}';
        }
}
